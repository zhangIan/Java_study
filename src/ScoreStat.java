import java.util.Scanner;

public class ScoreStat {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("STB的成绩是：");
        int stb = input.nextInt();
        System.out.println("Java的成绩是：");
        int javaScore = input.nextInt();
        System.out.println("SQL的成绩是：");
        int sqlScore = input.nextInt();

        int diff;
        double avg;

        diff = javaScore - sqlScore;

        avg = (stb + javaScore + sqlScore) / 3;

        System.out.println("Java 和Sql的成绩差是：" + diff);
        System.out.println("3门课程的平均分是：" + avg);
    }
}
